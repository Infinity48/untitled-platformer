<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="Test" tilewidth="128" tileheight="128" tilecount="5" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="32" height="32" source="images/wall.png"/>
  <objectgroup draworder="index" id="2">
   <object id="2" x="0" y="0" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <image width="128" height="128" source="images/pig.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="13" y="16" width="96" height="96"/>
  </objectgroup>
 </tile>
 <tile id="11">
  <image width="32" height="32" source="images/coin.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="4" y="3" width="24" height="24"/>
  </objectgroup>
 </tile>
 <tile id="12">
  <image width="32" height="32" source="images/enemy.png"/>
  <objectgroup draworder="index" id="2">
   <object id="1" x="1" y="1" width="30" height="30"/>
  </objectgroup>
 </tile>
 <tile id="13">
  <image width="32" height="32" source="images/goal.png"/>
  <objectgroup draworder="index" id="4">
   <object id="5" x="3.81818" y="20.9091">
    <polygon points="0,0 0.181818,-19.8182 23.9091,-19.8182 24.3636,11 22.0909,11 22,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
