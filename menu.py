''' (UNTITLED)
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
from consts import *
from game import *

class TitleScreen(arcade.View):
	class PlayButton(arcade.gui.UIFlatButton):
		def __init__(self):
			super().__init__('Play', center_x=320, center_y=160, width=250)
			
		def on_click(self):
			window.show_view(Game())
	
	def __init__(self):
		super().__init__()
		self.ui_manager = arcade.gui.UIManager()
		arcade.gui.ui_style.UIStyle.default_style().set_class_attrs(None, font_name='fonts/DejaVuSans', font_size=20)
	
	def setup(self):
		self.ui_manager.purge_ui_elements()
		self.ui_manager.add_ui_element(self.PlayButton())
	
	def on_show_view(self):
		self.setup()
		arcade.set_background_color((100,149,237))
	
	def on_hide_view(self):
		self.ui_manager.unregister_handlers()
	
	def on_draw(self):
		arcade.start_render()