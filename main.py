''' (UNTITLED)
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
from consts import *
from menu import *

os.chdir('assets')

def main():
	""" Main method """
	window.show_view(TitleScreen())
	arcade.run()

if __name__ == "__main__":
	main()