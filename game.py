''' (UNTITLED)
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
from consts import *

class Missile(arcade.Sprite):
		def __init__(self, sx, sy):
			super().__init__('images/verygoodcircle.png', 0.5)
			self.center_x = sx
			self.center_y = sy
			self.change_x = 0
			self.change_y = 0
			
		def update(self, cam_x, cam_y):
			self.center_x += self.change_x
			self.center_y += self.change_y
			if self.center_x < cam_x or self.center_x > cam_x + SCREEN_WIDTH or self.center_y < cam_y or self.center_y > cam_y + SCREEN_HEIGHT:
				self.kill()

class Pause(arcade.View):
		def __init__(self, prevview):
			super().__init__()
			self.prevview = prevview
		
		def on_draw(self):
			arcade.start_render()
			arcade.draw_text('Paused\nPress Enter to return\nPress Esc to quit', SCREEN_WIDTH/2, SCREEN_HEIGHT/2, arcade.color.WHITE,
														font_name='fonts/DejaVuSans', font_size=15, anchor_x='center', align='center')
		
		def on_key_release(self, key, modifiers):
			if key == arcade.key.ENTER:
				window.show_view(self.prevview)
			elif key == arcade.key.ESCAPE:
				arcade.close_window()
		
		def on_show_view(self):
			arcade.set_viewport(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT)

class Game(arcade.View):
	"""
	Main application class.
	"""

	def __init__(self):
		super().__init__()

		self.coin_list = None
		self.wall_list = None
		self.missile_list = arcade.SpriteList()
		self.levels = ['levels/boxprison.tmx', 'levels/level1.tmx', 'levels/level2.tmx']
		self.level = 0
		self.starting_score = 0
		self.score = 0

		self.player = arcade.Sprite(':resources:images/animated_characters/female_adventurer/femaleAdventurer_idle.png', SPRITE_SCALING/2)
		self.setup()

	def setup(self):
		self.player.center_x = 64
		self.player.center_y = 80
		
		self.camera = util.SimpleCamera(SCREEN_WIDTH, SCREEN_HEIGHT)
		self.dialogue = util.SpeechBox(800, 100, 500, 100, 10)
		try:
			self.dialogue.load(self.level)
		except ValueError:
			pass
		
		self.score = self.starting_score
		
		self.missile_list = arcade.SpriteList()

		map = arcade.tilemap.read_tmx(self.levels[self.level])
		
		self.wall_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Walls',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
		self.coin_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Coins',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True)
		self.enemy_list = arcade.tilemap.process_layer(map_object=map,
														layer_name='Enemies',
														scaling=SPRITE_SCALING)
		self.goal = list(arcade.tilemap.process_layer(map_object=map,
														layer_name='Goal',
														scaling=SPRITE_SCALING,
														use_spatial_hash=True))[0]
		
		print(f'Loaded level {self.level}!')
		
		if map.background_color:
			arcade.set_background_color(map.background_color)

		# Create the 'physics engine'
		self.player.physics_engine = arcade.PhysicsEnginePlatformer(self.player,
																	self.wall_list,
																	GRAVITY)
		for enemy in self.enemy_list:
			enemy.physics_engine = arcade.PhysicsEnginePlatformer(enemy,
																  self.wall_list,
																  GRAVITY)

	def on_draw(self):
		""" Render the screen. """

		# Clear the screen to the background color
		arcade.start_render()

		# Draw our sprites
		self.wall_list.draw()
		self.coin_list.draw()
		self.enemy_list.draw()
		self.missile_list.draw()
		self.goal.draw()
		self.player.draw()
		self.dialogue.draw()
		
		if DEBUG:
			for enemy in self.enemy_list:
				if arcade.has_line_of_sight(self.player.position, enemy.position, self.wall_list):
					arcade.draw_line(self.player.center_x, self.player.center_y, enemy.center_x, enemy.center_y, arcade.color.GREEN, 2)
				else:
					arcade.draw_line(self.player.center_x, self.player.center_y, enemy.center_x, enemy.center_y, arcade.color.RED, 2)
			self.wall_list.draw_hit_boxes()
			self.coin_list.draw_hit_boxes()
			self.enemy_list.draw_hit_boxes()
			self.missile_list.draw_hit_boxes()
			self.goal.draw_hit_box()
			self.player.draw_hit_box()
		
		arcade.draw_text(f'Score: {self.score}', self.camera.x+20, self.camera.y+25, arcade.color.WHITE, font_name='fonts/DejaVuSans', font_size=10)

	def on_key_press(self, key, modifiers):
		"""Called whenever a key is pressed. """
		if not self.dialogue.show:
			if key == arcade.key.SPACE or key == arcade.key.W:
				if self.player.physics_engine.can_jump():
					self.player.change_y = PLAYER_JUMP_SPEED
			elif key == arcade.key.A:
				self.player.change_x = -PLAYER_MOVEMENT_SPEED
			elif key == arcade.key.D:
				self.player.change_x = PLAYER_MOVEMENT_SPEED
			elif key == arcade.key.J:
				missile = Missile(self.player.center_x, self.player.center_y)
				missile.change_x = -8
				self.missile_list.append(missile)
			elif key == arcade.key.L:
				missile = Missile(self.player.center_x, self.player.center_y)
				missile.change_x = 8
				self.missile_list.append(missile)
			elif key == arcade.key.I:
				missile = Missile(self.player.center_x, self.player.center_y)
				missile.change_y = 8
				self.missile_list.append(missile)
			elif key == arcade.key.K:
				missile = Missile(self.player.center_x, self.player.center_y)
				missile.change_y = -8
				self.missile_list.append(missile)
		if key == arcade.key.F11:
			window.set_fullscreen(not window.fullscreen)

	def on_key_release(self, key, modifiers):
		"""Called when the user releases a key. """

		if key == arcade.key.LEFT or key == arcade.key.A:
			self.player.change_x = 0
		elif key == arcade.key.RIGHT or key == arcade.key.D:
			self.player.change_x = 0
		elif key == arcade.key.SPACE:
			self.dialogue.next()
		elif key == arcade.key.ESCAPE:
			window.show_view(Pause(self))

	def on_update(self, delta_time):
		""" Movement and game logic """
		
		self.camera.update()
		self.dialogue.update(delta_time)
		if self.player.center_x >= SCREEN_WIDTH / 2:
			self.camera.x = self.player.center_x - SCREEN_WIDTH / 2
		if self.player.center_y >= SCREEN_HEIGHT / 2:
			self.camera.y = self.player.center_y - SCREEN_HEIGHT / 2
		if self.player.center_y < 0:
			self.setup()
		self.dialogue.center_x = self.camera.x + SCREEN_WIDTH / 2
		self.dialogue.center_y = self.camera.y + 105

		if not self.dialogue.show:
			# Move the player with the physics engine
			self.player.physics_engine.update()
			for enemy in self.enemy_list:
				enemy.physics_engine.update()
				if arcade.check_for_collision_with_list(enemy, self.missile_list):
					enemy.kill()
					self.score += 5
				elif arcade.check_for_collision(self.player, enemy):
					if self.player.change_y < 0:
						self.player.change_y = 15
						enemy.kill()
						self.score += 5
					else:
						self.setup()
			for missile in self.missile_list:
				missile.update(self.camera.x, self.camera.y)
				if arcade.check_for_collision_with_list(missile, self.wall_list):
					missile.kill()
			for coin in self.coin_list:
				if arcade.check_for_collision(coin, self.player):
					self.score += 1
					coin.kill()
			for enemy in self.enemy_list:
				if arcade.has_line_of_sight(self.player.position, enemy.position, self.wall_list):
					if random.randrange(4) == 0:
						if self.player.center_x - enemy.center_x < 0:
							enemy.change_x = -ENEMY_MOVEMENT_SPEED
						else:
							enemy.change_x = ENEMY_MOVEMENT_SPEED
				else:
					if round(enemy.change_x, 2) > 0:
						enemy.change_x -= 0.05
					elif round(enemy.change_x, 2) < 0:
						enemy.change_x += 0.05

			if arcade.check_for_collision(self.player, self.goal):
				self.level += 1
				self.starting_score = self.score
				self.setup()
	
	def on_show_view(self):
		self.camera.update()