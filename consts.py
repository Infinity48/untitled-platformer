''' (UNTITLED)
	Copyright (C) 2021 Infinity48

	This Source Code Form is subject to the terms of the Mozilla Public
	License, v. 2.0. If a copy of the MPL was not distributed with this
	file, You can obtain one at https://mozilla.org/MPL/2.0/.
'''
import arcade, arcade.gui, util, os, random

# Constants
SCREEN_WIDTH = 960
SCREEN_HEIGHT = 540
SCREEN_TITLE = "Platformer"
DEBUG = False

SPRITE_SCALING = 1

# Movement speed of player, in pixels per frame
PLAYER_MOVEMENT_SPEED = 5
GRAVITY = 1.25
PLAYER_JUMP_SPEED = 20
ENEMY_MOVEMENT_SPEED = 1

window = arcade.Window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE, fullscreen=False, antialiasing=False)